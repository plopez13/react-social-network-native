import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, TextInput, ScrollView } from 'react-native';
import MainScreen from './components/MainScreen';
import FeedScreen from './components/FeedScreen';
import InfoScreen from './components/InfoScript';
import { useState } from 'react';




export default function App() {

  const [mostrar,setMostrar]= useState("Info")

  return (


    <View>
      <Image style={styles.MainScreen__Img}
        source={require('./assets/MainScreen/Hannah.jpg')} />

      <ScrollView>

        <MainScreen setMostrar={setMostrar}/>
        
        
        {
          mostrar == "Info" &&
          <InfoScreen/>
        }

        {
          mostrar == "Feed" &&
          <FeedScreen/>
        }
      
      </ScrollView>
    </View>

  );
}

const styles = StyleSheet.create({


  MainScreen__Img: {

    height: 926,
    width: 428,
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: -1,
    backgroundColor: "#f0f"
  },
})
