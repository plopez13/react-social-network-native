import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  FlatList,
} from "react-native";

export default function FeedScreen() {
  const [fotos, setfotos] = useState([
    {
      id: 1,
      link: require("./../assets/1.jpg"),
    },
    {
      id: 2,
      link: require("./../assets/2.jpg"),
    },
    {
      id: 3,
      link: require("./../assets/3.jpg"),
    },
    {
      id: 4,
      link: require("./../assets/4.jpg"),
    },
    {
      id: 5,
      link: require("./../assets/5.jpg"),
    },
    {
      id: 6,
      link: require("./../assets/6.jpg"),
    },
    {
      id: 7,
      link: require("./../assets/7.jpg"),
    },

    {
      id: 8,
      link: require("./../assets/8.jpg"),
    },
    {
      id: 9,
      link: require("./../assets/9.jpg"),
    },
    {
      id: 10,
      link: require("./../assets/10.jpg"),
    },
  ]);

  return (
    <>
      <ScrollView style={styles.box}>
        {/* <Text> estoy en fe</Text> */}
        <FlatList
          data={fotos}
          numColumns={2}
          renderItem={({ item }) => (
            <View>
              <Image style={styles.box__Images} source={item.link} />
            </View>
          )}
        />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  box: {
    backgroundColor: "#38373a",

    /* justifyContent: "center",
    alignItems: "center", */
    padding: 5,
    position: "relative",
    marginTop: 5,
    borderRadius: 20,
  },
  box__Images: {
    borderRadius: 26,
    width: 180,

    margin: 10,
  },
  /*  afterElement: {
    content: '""',
    width: 82,
    height: 82,
    backgroundColor: "#38373a",
    position: "absolute",
    top: -40,
    left: "calc(50% - 41)",
    transform: "rotate(45deg)",
    borderRadius: 21,
    zIndex: -1,
  }, */
});
