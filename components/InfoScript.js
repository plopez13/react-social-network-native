import { View, Text, StyleSheet, Image, Pressable } from 'react-native';
import { useState } from 'react';
import Instagram from "./../assets/InfoScript/Instagram.jpg";
import Twitter from "../assets/InfoScript/Twitter.jpg";
import Youtube from "../assets/InfoScript/Youtube.jpg";
import Facebook from "../assets/InfoScript/Facebook.jpg";
import Telegram from "../assets/InfoScript/Telegram.jpg";
import { YellowBox } from 'react-native-web';

export default function InfoScript() {
  

  const [redes, setredes] = useState([
    {
      red: "Instagram",
      link: Instagram,
      hashtag: "@hannah_super",
      estilo: "naranja",
    },
    {
      red: "Twitter",
      link: Twitter,
      hashtag: "@hannah_super",
      estilo: "azul",
    },
    {
      red: "Youtube",
      link: Youtube,
      hashtag: "@hannah_super",
      estilo: "rojo",
    },
    {
      red: "Facebook",
      link: Facebook,
      hashtag: "",
      estilo: "azul",
    },
    {
      red: "Telegram",
      link: Telegram,
      hashtag: "@hannah_super",
      estilo: "azul",
    },
  ]);

  function className(params) {
    let classN = `${Styles.boton} `;
    if (params === "rojo") {
        classN += `${Styles.boton__rojo}`;
    } else if (params === "naranja") {
      classN += `${Styles.boton__naranja}`;
    } else {
        classN += `${Styles.boton__azul}`;
    }

    return classN;
}

  

  const [mostrar, setMostrar] = useState("info")

  return (
      <>
        <View style={Styles.contenedor}>
          <View style={Styles.contenedor__cont}>
            <View style={Styles.contenedor__cont__about}>About</View>
            <Text style={Styles.contenedor__cont__h5}>👜 Fashion, beauty, lifestyle & aesthetic inspo </Text>
            <Text style={Styles.contenedor__cont__h5}>🙌 Founder of <Text style={Styles.contenedor__cont__beatyhstudio}>@beatyhstudio</Text> </Text>
            <Text style={Styles.contenedor__cont__h5}>📍 Berlin</Text>
          </View>
          
          <View style={Styles.container}>
          <View style={Styles.botones}>
            {redes.map((red, index) => (
              <Pressable style = {[Styles.boton ,red.estilo == "rojo"? Styles.boton__rojo: red.estilo == "azul" ? Styles.boton__azul: Styles.boton__naranja ] }>
                <Image style={Styles.imagen} source={red.link} alt="imagen de producto"></Image>
                <Text style={Styles.botones__red}> {red.red}</Text>
                <Text style={Styles.hashtag}> {red.hashtag}</Text>
              </Pressable>
            ))}
          </View>
        </View>
        </View>
      </>
    );
  }


const Styles = StyleSheet.create ({
  container: {
    flex: 1,
    width: 428,
    height: 614,
    backgroundColor: "#27262A",
    
    
  },
  boton__rojo: {
    borderWidth: 1,
    borderColor: "rgba(255, 58, 58, 0.54)",
    backgroundcolor: "#27262A",
},

boton__azul: {
  borderWidth: 1,
  borderColor: 'rgba(41, 152, 255, 0.74)',
  backgroundColor: '#27262A',
},

boton__naranja: {
  borderWidth: 1,
  borderColor: 'rgba(255, 229, 89, 0.38)',
  backgroundColor: '#27262A',
},

contenedor: {
  backgroundColor: "#27262A",
},

contenedor__cont: {
  display: "inline-flex",
  flexdirection: "column",
  alignitems: "flex-start",
  gap: 4,
  margin: 20,
  marginHorizontal: 30,
  width: 344,
},

contenedor__cont__about: {
  color: "#888",
  fontfamily: "Be Vietnam",
  fontsize: "16",
  fontstyle: "normal",
  fontweight: "400",
  lineheight: "18", /* 112.5% */
},

contenedor__cont__beatyhstudio: {
  color: "#5588EB",
},

contenedor__cont__h5: {
  color: 'white',
  textAlign: 'left',
  fontFamily: 'Be Vietnam',
  fontSize: 16,
  fontStyle: 'normal',
  fontWeight: '400',
  lineHeight: 20,
  height: 20,
},

boton: {
  width: 374,
  height: 60,
  padding: 8,
  paddingHorizontal: 46,
  paddingVertical: 12,
  borderRadius: 9,
  justifyContent: 'space-between',
  flexDirection: "row",
  alignItems: "center",
  
},

botones: {
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  gap: 12,
  backgroundColor: "#27262A",
  
    
},

botones__red: {
  color: 'white',
  },

hashtag: {
  color: '#888',
  textAlign: "center",  
  justifyContent: "center",
},

imagen: {
  width: 30,
  height: 30,  
  display: "inline-block",
  color: "red",
},

})



