
import { StyleSheet, Text, View, Image, Pressable } from "react-native";


export default function MainScreen({setMostrar}) {


  return (
    

    <View style={styles.MainScreen}>
    
            
      <Text style={styles.Texto__Nombre}>Hannah Smith</Text>
      <Text style={styles.Texto__Usuario}>@Hannah_super</Text>
              
      <View style={styles.MainScreen__Follow__Menu}>

        <Pressable style={styles.Button__Follow}>
          <Text style={styles.Texto__Button__Follow}>+Follow</Text>
        </Pressable>
          
        <Pressable >
          
          <Image style={styles.Button__Send} source={require('./../assets/MainScreen/send.jpg')} />
        
        </Pressable>
          
        <View style={styles.MainScreen__Follow__Menu}>
            <View style={styles.MainScreen__Follow__Menu__Folowers__Following}>
                <View >
                  <Text style={styles.Texto__Num__Followers__Following}>787</Text>
                    
                </View>

                <View >
                  <Text style={styles.Texto__Followers__Following}>Followers</Text>
                    
                </View>
            </View>

            <View style={styles.MainScreen__Follow__Menu__Folowers__Following}>
                <View >
                  <Text style={styles.Texto__Num__Followers__Following}>787</Text>
                    
                </View>

                <View >
                  <Text style={styles.Texto__Followers__Following}>Following</Text>
                    
                </View>
            </View>            
               
        </View>

        <Pressable >
          <Image style={styles.Button__More} source={require('./../assets/MainScreen/more.jpg')} />
        </Pressable>

      </View>

      <View style={styles.MainScreen__Follow__Info__Menu}>

        <Pressable style={styles.Button__Info__Menu} onPress={()=> setMostrar("Info")}>
          <Text style={styles.Texto__Button__Info__Menu}>Info</Text>
        </Pressable>
        
        <Pressable style={styles.Button__Info__Menu} onPress={()=> setMostrar("Feed")}>
          <Text style={styles.Texto__Button__Info__Menu}>Feed</Text>
        </Pressable>

        <Pressable style={styles.Button__Info__Menu}>
          <Text style={styles.Texto__Button__Info__Menu}>Tagged</Text>
        </Pressable>

      </View>
    </View>


  )
}

const styles = StyleSheet.create({

  MainScreen: {
    flex: 1,
    backgroundColor: '#f61616',
    width: 428,
    marginTop: 800,
    marginBottom:1000,
  },

  MainScreen__Follow__Menu: {
    flex: 0.5,
    width: 428,

    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#888888',
  },

  MainScreen__Follow__Menu__Folowers__Following:{
    width: 70,
    flexDirection:'column',
    justifyContent: 'center',
    alignContent: 'center',
  },

  MainScreen__Follow__Info__Menu: {
    flex: 0.5,
    width: 428,

    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#888888',
  },

  Texto: {
    flex: 1,
    backgroundColor: '#f61616',
    
  },

  Texto__Nombre: {
    height:50,
    fontSize:40,
    paddingTop: 5,
    paddingStart:5,
    fontFamily: 'Be Vietnam',
    color: '#ffffff', 
    fontWeight: 700,
    backgroundColor: '#888888',
    
  },

  Texto__Usuario: {
    
    height:20,
    color: '#38373A',
    fontWeight: 800,
    paddingStart: 5,
    backgroundColor: '#888888',
    
  },

  Button__Follow:{

    backgroundColor:'#38373A',
    borderRadius: 10,
    paddingHorizontal: 40 ,
    paddingVertical: 10 , 
    marginBottom: 10,
    marginTop:10,
  },

  Texto__Button__Follow:{

    fontSize:15,
    fontWeight:400,
    color:'#5992FF',

  },

  Button__Send:{

    width: 35,
    height: 35,
    backgroundColor:'#38373A',
    borderRadius: 10,
    padding: 15, 
    
  },

  Texto__Num__Followers__Following:{

    color:'#ffffff',
    fontWeight:600,

  },

  Texto__Followers__Following:{

    color:'#38373A',
    fontWeight:600,

  },

  Button__More:{

    width: 35,
    height: 35,
    backgroundColor:'#38373A',
    borderRadius: 10,
    padding: 15, 
    
  },

  Button__Info__Menu:{

    backgroundColor:'#38373A',
    borderRadius: 5,
    paddingHorizontal: 40 ,
    paddingVertical: 10 , 
    
  },

  Texto__Button__Info__Menu:{

    color:'#ffffff',
    fontSize:15,
    fontWeight:400, 
    
  },
})

